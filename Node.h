#ifndef NODE
#define NODE

class  Node{
public:
    Node(double h, double k){
        this->h = h;
        this->k = k;
        left = nullptr ;
        right = nullptr;
        top = nullptr;
        down = nullptr;
        merged = false;
    }

    bool isMerged(){
        return merged;
    }

    void merge(){
        merged = true;
    }

    void divide(){
        merged = false;
    }

    bool checkForMerge(Node *node){
        if(!merged && node && !node->isMerged() && h < node->getH()*0.8 && k+node->getK() < 2){
            merged = true;
            node->merge();
            return true;
        }
        return false;
    }

    double getH() const {
        return h;
    }

    double getK() const {
        return k;
    }

    Node *getLeft() const {
        return left;
    }

    Node *getRight() const {
        return right;
    }

    Node *getTop() const {
        return top;
    }

    Node *getDown() const {
        return down;
    }

    void setH(double h) {
        Node::h = h;
    }

    void setK(double k) {
        Node::k = k;
    }

    void setMerged(bool merged) {
        Node::merged = merged;
    }

    void setLeft(Node *left) {
        Node::left = left;
    }

    void setRight(Node *right) {
        Node::right = right;
    }

    void setTop(Node *top) {
        Node::top = top;
    }

    void setDown(Node *down) {
        Node::down = down;
    }

private:
    double h;
    double k;
    bool merged;
    Node *left;
    Node *right;
    Node *top;
    Node *down;
};

#endif