#include <iostream>
#include <vector>
#include <math.h>
#include "randomnumbergenerator.h"
#include "protein.h"
#include "Node.h"
#include "mergedProtein.h"

std::vector<Node*> generateRandomProteins(int proteinNumber,double minimumVariation, double maximumVariation){
    std::vector<Node*> proteins;
    RandomNumberGenerator* rng = new RandomNumberGenerator();

    for (int i = 0; i < proteinNumber; i++) {
        Node *protein = new Node(rng->getDouble(minimumVariation, maximumVariation),
        rng->getDouble(minimumVariation, maximumVariation));
        proteins.push_back(protein);
    }
    delete rng;
    return proteins;
}

std::vector<Node*> generateProteinGraph(int proteinNumber,double minimumVariation, double maximumVariation){
    std::vector<Node*> proteins = generateRandomProteins(proteinNumber, minimumVariation, maximumVariation);
    int columns = static_cast<int>(std::sqrt(proteinNumber));
    for(int i = 0; i < columns-1; i++){
        proteins[i]->setRight(proteins[i+1]);
        proteins[i+1]->setLeft(proteins[i]);
    }
    int column = columns+1;
    int row = 0;
    for(int columnEnd = columns; columnEnd<proteins.size()-columns;columnEnd+=columns){
        columnEnd -=1;
        for(; row<columnEnd; row++, column++){
            proteins[row]->setDown(proteins[column]);
            proteins[column]->setTop(proteins[row]);
            proteins[column]->setRight(proteins[column+1]);
            proteins[column+1]->setLeft(proteins[column]);
        }
        columnEnd += 1;
        proteins[columnEnd]->setDown(proteins[column]);
        proteins[column]->setTop(proteins[columnEnd]);
    }
    return proteins;
}

void disorder(std::vector<Node*> *graph){
    RandomNumberGenerator* rng = new RandomNumberGenerator();
    unsigned int lastIndex = graph->size()-1;
    unsigned int halfSize = graph->size()/ 2;
    for( unsigned int counter = 0; counter < halfSize; counter++){
        unsigned int take = rng->getInt(0, lastIndex);
        unsigned int place = rng->getInt(0, lastIndex);
        std::swap(graph->at(take), graph->at(place));
    }
    delete rng;
}

int main(){
    int maxProtein = 100000;
    int iterations = 100;
    double minVariation = 0.2;
    double maxVariation = 2.6;
    RandomNumberGenerator* rng = new RandomNumberGenerator();

    std::vector<Node*> graph = generateProteinGraph(maxProtein, minVariation, maxVariation);
    std::vector<MergedProtein*> mergedProteins;
    for(int epoch = 0; epoch<iterations; epoch++){
        for(int index = 0; index < maxProtein; index++){
            Node* protein = graph[index];
            if(protein->checkForMerge(protein->getLeft())){
                mergedProteins.push_back(new MergedProtein(protein, protein->getLeft(), 0));
            }else if(protein->checkForMerge(protein->getRight())){
                mergedProteins.push_back(new MergedProtein(protein, protein->getRight(), 1));
            }else if(protein->checkForMerge(protein->getTop())){
                mergedProteins.push_back(new MergedProtein(protein, protein->getTop(), 2));
            }else if(protein->checkForMerge(protein->getDown())){
                mergedProteins.push_back(new MergedProtein(protein, protein->getDown(), 3));
            }
        }
        for(int index = 0; index<mergedProteins.size(); index++){
            MergedProtein* mergedProtein = mergedProteins[index];
            mergedProtein->first->divide();
            mergedProtein->second->divide();
            Node * newNode = new Node(rng->getDouble(minVariation, maxVariation),
                                      rng->getDouble(minVariation, maxVariation));
            switch(mergedProtein->mergeType){
                case 0:
                    mergedProtein->first->setLeft(newNode);
                    newNode->setRight(mergedProtein->first);
                    mergedProtein->second->setRight(newNode);
                    newNode->setLeft(mergedProtein->second);
                    break;
                case 1:
                    mergedProtein->first->setRight(newNode);
                    newNode->setLeft(mergedProtein->first);
                    mergedProtein->second->setLeft(newNode);
                    newNode->setRight(mergedProtein->second);
                    break;
                case 2:
                    mergedProtein->first->setTop(newNode);
                    newNode->setDown(mergedProtein->first);
                    mergedProtein->second->setDown(newNode);
                    newNode->setTop(mergedProtein->second);
                    break;
                default:
                    mergedProtein->first->setDown(newNode);
                    newNode->setTop(mergedProtein->first);
                    mergedProtein->second->setTop(newNode);
                    newNode->setDown(mergedProtein->second);
                    break;
            }
            graph.push_back(newNode);
        }
        mergedProteins.clear();
    }
    return 0;
}
