#ifndef MERGE_PROTEIN
#define MERGE_PROTEIN
#include "Node.h"

struct MergedProtein{
    MergedProtein(Node *first, Node *second, int mergeType) {
        this->first = first;
        this->second = second;
        this->mergeType = mergeType;
    }

    Node *first;
    Node *second;
    char mergeType;
};

#endif