#ifndef RANDOMNUMBERGENERATOR_H
#define RANDOMNUMBERGENERATOR_H

#include "random"

struct RandomNumberGenerator{
    unsigned int getInt(unsigned int min, unsigned int max){
        std::random_device random;
        std::mt19937 rng(random());
        std::uniform_int_distribution<std::mt19937::result_type> number(min, max);
        return number(rng);
    }
    bool getBool(){
        return getInt(0, 1);
    }

    double getDouble(double min, double max){
        static std::default_random_engine engine;
        static std::uniform_real_distribution<> distribution(min, max);
        return distribution(engine);
    }
};

#endif // RANDOMNUMBERGENERATOR_H
